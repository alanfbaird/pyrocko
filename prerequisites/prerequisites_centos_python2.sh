#!/bin/bash

sudo yum -y install make gcc patch git python python-yaml python-matplotlib numpy \
    scipy python-requests python-coverage python-nose python-jinja2 \
    PyQt4 python-matplotlib-qt4
