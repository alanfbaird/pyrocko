``modelling``
=============

``pyrocko.modelling.okada``
---------------------------

.. automodule:: pyrocko.modelling.okada
    :members:


``pyrocko.modelling.cracksol``
------------------------------

.. automodule:: pyrocko.modelling.cracksol
    :members:
