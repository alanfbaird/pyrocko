
Installation under Linux
------------------------

This section lists the commands needed to install Pyrocko and its prerequisites
on some popular Linux distributions

.. toctree::
   :maxdepth: 1

   Ubuntu, Debian, Mint, ... (deb based) <deb>
   Centos, Fedora, ... (rpm based) <centos>
   OpenSuse <suse>
   Arch <arch>

For instructions on how to install Pyrocko on other systems or if the
installation with any of the above procedures fails, see
:doc:`/install/details`.
